# OLA ride analysis
# By- Aarush Kumar
In this project I have used seaborn & matplotltib libraries of Python.
OLA is one of the very popular taxi services in the world. It serves millions of travelers in different cities daily. The data visualization project with matplotltib can be used along with the Python language for creating a Data Science Project. The use of libraries of Python language and analysis of parameters like the trips by hours, number of trips in a day and total trips made by single taxi in a month, quarter, half-yearly or yearly basis can be taken into consideration.I used OLA pickup services used by travelers in a particular region and thereby create a visualization of different time frames during a year. The result found will reveal the impact of the time factor on customer trips.
Thankyou!
