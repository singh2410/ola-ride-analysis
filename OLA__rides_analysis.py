#!/usr/bin/env python
# coding: utf-8

# # OLA rides analysis using Python & ML
# #By- Aarush Kumar
# #Dated: May 20,2021

# In[3]:


import pandas as pd
import numpy as np
import datetime
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns
matplotlib.style.use('ggplot')
import calendar


# In[4]:


df=pd.read_csv(r'/home/aarush100616/Downloads/Projects/Ola rides analysis/ola.csv')
df


# In[5]:


#cleaning data
df.tail()


# In[7]:


df=df[:-1]


# In[8]:


df.isnull().sum()


# In[11]:


sns.heatmap(df.isnull(),yticklabels=False,cmap="viridis")


# In[12]:


#remove null values
df=df.dropna()
sns.heatmap(df.isnull(),yticklabels=False,cmap="viridis")


# In[15]:


#transforming of data
df['START_DATE*'] = pd.to_datetime(df['START_DATE*'], format="%m/%d/%Y %H:%M")
df['END_DATE*'] = pd.to_datetime(df['END_DATE*'], format="%m/%d/%Y %H:%M")


# In[16]:


hour=[]
day=[]
dayofweek=[]
month=[]
weekday=[]
for x in df['START_DATE*']:
    hour.append(x.hour)
    day.append(x.day)
    dayofweek.append(x.dayofweek)
    month.append(x.month)
    weekday.append(calendar.day_name[dayofweek[-1]])
df['HOUR']=hour
df['DAY']=day
df['DAY_OF_WEEK']=dayofweek
df['MONTH']=month
df['WEEKDAY']=weekday


# In[17]:


#calculating travel time
time=[]
df['TRAVELLING_TIME']=df['END_DATE*']-df['START_DATE*']
for i in df['TRAVELLING_TIME']:
    time.append(i.seconds/60)
df['TRAVELLING_TIME']=time
df.head()


# In[18]:


#calculating the average speed of trip
df['TRAVELLING_TIME']=df['TRAVELLING_TIME']/60
df['SPEED']=df['MILES*']/df['TRAVELLING_TIME']
df.head()


# In[33]:


#visualization
x = df['CATEGORY*'].value_counts().plot(kind='bar',figsize=(13,8))


# In[37]:


df['MILES*'].plot.hist()


# In[30]:


df['PURPOSE*'].value_counts().plot(kind='bar',figsize=(13,8),color='red')


# In[29]:


df['HOUR'].value_counts().plot(kind='bar',figsize=(13,8),color='indigo')


# In[41]:


df['WEEKDAY'].value_counts().plot(kind='bar',color='grey',figsize=(13,8))


# In[43]:


df['DAY'].value_counts().plot(kind='bar',figsize=(15,5),color='skyblue')


# In[46]:


df['MONTH'].value_counts().plot(kind='bar',figsize=(13,8),color='green')


# In[48]:


df['START*'].value_counts().plot(kind='bar',figsize=(25,5),color='black')


# In[50]:


df.groupby('PURPOSE*').mean().plot(kind='bar',figsize=(15,5))

